function updatePage(result)
{
    var dom_list = $("<ul>");
    for(var i = 0; i < result.length; i++ )
    {
        var item = result[ i ];
        dom_list.append(
            $("<li>")
            .append(
                $("<h2>")
                .append(item.full_name)
            )

            .append(
                $("<p>")
                .append(
                        $("<span>")
                )
                .append("<b>Latitude:</b> " + item.geocode.latitude)
                .append(" | ")
                .append(
                        $("<span>")
                )
                .append("<b>Longitude:</b> " + item.geocode.longitude)
            )
            .append(
                $("<p>")
                .append("<b>Celsius:</b> " + item.info.celsius + " Cº")
            )
            .append(
                $("<p>")
                .append("<b>Fahrenheit:</b> " + item.info.fahrenheit + " Fª")
            )
        );
    }

    $("#content")
        .html(
           result.length ? dom_list : "No donuts for your"
        );
}

function fireInTheHole(evt){


    var query = $("#bullet").val();
    if( ! query ){
        alert("The search field can not be empty");
        return;
    }

    $("#content").html("Searching ...");

    $.ajax({
        url: '/forecast/' + query,
        success: function(data){
            var response = JSON.parse(data);
            updatePage(response.result);
        }
    });
}

$(function(){
    $("#trigger").click(fireInTheHole);
});
