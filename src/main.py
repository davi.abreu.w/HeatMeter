import sys

import heat_meter
import web
import time

def create_server():
    service = heat_meter.create_service()
    return web.Server(service)

server = None
retry = 0
max_retries = 5
retry_sleep = 2

while retry < max_retries and not server:
    try:
        server = create_server()
    except:
        print("Not possible start server. Trying ...")
        retry += 1
        time.sleep(retry_sleep)

if not server:
    raise Exception("Sorry, was not possible startup the application. Check if the data base is up.")
