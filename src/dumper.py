import os
import csv
from time import sleep
from threading import Thread
from datetime import date, datetime, timedelta
from random import randint

class Dumper():

    FILE = "last_dump.time"
    TIME_FORMAT = "%Y-%m-%d %H:%M"
    OFFSET_RANGE = 30
    DUMP_PERIOD_DAYS = timedelta(days=1)

    def __init__(self, persistence, hour_to_dump=23, minute_to_dump=59, offset=1):
        if not os.path.exists(Dumper.FILE):
            self.save_now()
        self.persistence = persistence

        self.hour_to_dump = hour_to_dump
        self.minute_to_dump = minute_to_dump
        self.offset = offset

        self.dumper_thread = Thread(target=self.__worker, name="Dumper", daemon=True)
        self.dumper_thread.start()

    def save_now(self):
        f = open(Dumper.FILE, "w")
        f.write(datetime.now().strftime(Dumper.TIME_FORMAT))
        f.close()

    def load_last(self):
        f = open(Dumper.FILE, "r")
        readed = f.read()
        f.close()
        return datetime.strptime(readed, Dumper.TIME_FORMAT)
        
    def __worker(self):
        while True:
            next_wake = self.__get_seconds_to_next_wake()
            print("Next dump in about {} seconds".format(next_wake))
            sleep(next_wake)
            self.__dump_csv_ip_consult_count()

    def __dump_csv_ip_consult_count(self):  
        filename = '{}.dump.csv'.format(str(date.today()))
        desync_time = randint(self.offset, self.offset+Dumper.OFFSET_RANGE)
        print("Dumping in {} seconds".format(desync_time))
        sleep(desync_time)
        if not os.path.exists(filename):
            self.save_now()
            print("Dumping now")
            with open(filename, 'w') as file:
                out = csv.writer(file)
                out.writerow(['ip', 'count'])
                ips_count_dict = self.persistence.get_all_ips_consult_count()
                for ip, count in ips_count_dict.items():
                    out.writerow([ip, count])

    def __get_seconds_to_next_wake(self):
        next_wake = self.load_last().replace(hour=self.hour_to_dump, minute=self.minute_to_dump)
        next_wake = next_wake + Dumper.DUMP_PERIOD_DAYS
        return int((next_wake - datetime.now()).total_seconds())
