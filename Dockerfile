FROM python:3.6

RUN mkdir -p /app/heat_meter/src
RUN mkdir -p /app/heat_meter/web

WORKDIR /app/heat_meter

COPY src src
COPY web web
COPY requirements.txt .
COPY gunicorn.conf .

RUN pip install -r requirements.txt

CMD ["gunicorn", "--config", "gunicorn.conf", "--pythonpath", "src", "main:server"]
